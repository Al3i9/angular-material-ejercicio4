import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  formulario!: FormGroup;
  informacion: string[] = []

  constructor(private fb: FormBuilder) { 
    this.crearFormulario();
  }

  ngOnInit(): void {
  }

  crearFormulario(): void {
    this.formulario = this.fb.group({
      cajaG: [''],
      cajaP: this.fb.array([[]])
    })
  }

  get contenidoP(){
    return this.formulario.get('cajaP') as FormArray
  }

  agregar(): void{
    this.contenidoP.push(this.fb.control('',Validators.required))
  }

  borrarCajadinamica(i: number): void{
    this.contenidoP.removeAt(i);
    this.formulario.value.cajaP[i].reset
  }

  limpiarPrimeracaja(i: number){
    console.log(this.formulario.value.cajaP[i]);
    this.formulario.value.cajaP[i]
    
  }

  limpiar():void{
    this.informacion = ['']
    this.formulario.reset
  }

   limpiarTodo(): void{
    this.informacion = this.formulario.value.cajaP.join('\n')

   }

   guardar(): void{
     console.log(this.formulario.value.cajaP);
   }

}
